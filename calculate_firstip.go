package nethelp

import (
	"net"
	"strconv"
	"strings"
)

// CalculateFirstIP uses binary operations to calculate the broadcast address of a network
func CalculateFirstIP(ipNet net.IPNet) net.IP {
	var broadcastAddress = make([]byte, len(ipNet.IP))
	for index, b := range ipNet.Mask {
		tmp := ""
		ipParts := strings.Split(strconv.FormatInt(int64(ipNet.IP[index]), 2), "")
		maskParts := strings.Split(strconv.FormatInt(int64(b), 2), "")
		for len(ipParts) != 8 {
			ipParts = append([]string{"0"}, ipParts...)
		}
		for len(maskParts) != 8 {
			maskParts = append([]string{"0"}, maskParts...)
		}
		for i, part := range maskParts {
			if part == "1" {
				tmp += ipParts[i]
			} else {
				if i == len(maskParts)-1 && index == len(broadcastAddress)-1 {
					tmp += "1"
				} else {
					tmp += "0"
				}
			}
		}
		test, _ := strconv.ParseInt(tmp, 2, 64)
		broadcastAddress[index] = byte(test)
	}
	return net.IP(broadcastAddress)
}
