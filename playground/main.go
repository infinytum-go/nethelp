package main

import "gitlab.com/infinytum-go/nethelp"

import "net"

import "fmt"

func main() {

	//ip, theNet, err := net.ParseCIDR("2a00:1128:1:1::136:136/64")
	ip, theNet, err := net.ParseCIDR("192.168.1.128/25")
	if err != nil {
		return
	}

	fmt.Println("IP", ip)
	fmt.Println("Net", theNet.IP, theNet.Mask.String())

	fmt.Println(nethelp.CalculateBroadcast(*theNet).String())
	fmt.Println(nethelp.CalculateLastIP(*theNet).String())
	fmt.Println(nethelp.CalculateFirstIP(*theNet).String())
}
